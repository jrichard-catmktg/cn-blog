#!/usr/bin/env node
'use strict';

// Opens a specified URL in the browser

const opn = require('opn');
const arg = require('yargs').argv;

if (arg.dest) {
  console.log(arg.dest);
  opn(arg.dest);
} else {
  console.log("ERROR:  No destination URL was specified.");
}
process.exit();