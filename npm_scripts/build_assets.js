#!/usr/bin/env node
'use strict';

const argv = require('yargs').argv;
const js_compiler = require('./javascript_compiler');
const css_compiler = require('./css_compiler');


js_compiler({
  uncompressed: argv.hasOwnProperty('debug'),
  watch: argv.hasOwnProperty('watch')
});


css_compiler({
  debug: argv.hasOwnProperty('debug'),
  watch: argv.hasOwnProperty('watch')
});