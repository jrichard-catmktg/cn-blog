#!/usr/bin/env node
'use strict';

// DOC: javascript_compiler
//
// Compiles javascript using Gulp and saves the
// output in /web/assets/js.
//
// TODO: 
//   * Add support for multiple output files.
//   * This could be made more flexible by
//     building gulp tasks dynamically.
//
// USAGE:
//    CLI: $ node ./npm_scripts/javascript_compiler --uncompressed (optional)
//
//    const js = require(./javascript_compiler);
//    js({
//         uncompressed: true (optional) 
//    });
//
// OPTIONS:
//    uncompressed (boolean) (optional) Setting to true will prevent JS from getting minified.
//    fast         (boolean) (optional) Setting to true will prevent JS from being minified or
//                                      transpiled to ECMAScript Standard.
//    watch        (boolean) (optional) Watches JS files in the project's dev assets folder for changes.


// ----------  Setup  ---------- //

const argv = require('yargs').argv; // for parameters to this script
const path = require('path'); // for resolving absolute paths
const colors = require('colors'); // colorizing console output
const watcher = require('node-watch'); // for watching JS for changes
const project = require('./project_config.js'); // configuration specific to this project
const logger = require('./utils/logger.js'); // standardizes console logs
const del = require('del'); // for deleting the tmp directory
const gulp = require('gulp'); // task runner for coordinating compile tasks
const babel = require('gulp-babel'); // transpiles ES6 down to ES5
const concat = require('gulp-concat'); // combines files
const uglify = require('gulp-uglify'); // minifies files
const sequence = require('run-sequence'); // run gulp tasks consecutively

// Import js file map
const js_files = require( path.resolve(__dirname, path.join(project.assets.dev, 'js', 'root.js')) );

// Generates full source paths for gulp tasks
function get_paths ( _search_tmp ) { // _search_tmp = true ? Search the tmp directory : Use only unprocessed files
  var _dynamic = [];
  if ( _search_tmp ) {
    _dynamic = [ path.resolve(__dirname, path.join(project.assets.dev, 'js', 'tmp', '**', '*')) + '.js' ];
  } else {
    _dynamic = [
      path.resolve(__dirname, path.join(project.assets.dev, 'js', 'vanilla', '**', '*')) + '.js'
    ];
  }

  var _paths = [];
  _paths = _paths.concat(
    js_files.vendors, 
    _dynamic,
    js_files.angular.directives,
    js_files.angular.app
  );
  return _paths;
}


// Base configuration with default values
var config = {
  build_variant: 'build_js_default', // the build task that Gulp should use
  output_name: 'root.js'
}


// ----------  CLI  ---------- //

logger('JavaScript', 'Compiling JavaScript', 'green'); // Say hello

// Map argv properties to config properties

// Concatenated and transpiled but not minified
if (argv.hasOwnProperty('uncompressed')) {
  config.build_variant = 'build_js_uncompressed';
  logger('JavaScript', 'Output will be transpiled down to ECMAScript Standard, but not minified.', 'yellow');
}

// Concatenated only
if (argv.hasOwnProperty('fast')) {
  config.build_variant = 'build_js_fast';
  logger('JavaScript',
    '\r\n' +
    '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\r\n' +
    'WARNING:  Javascript will not be transpiled down to ECMAScript Standard.\r\n' +
    'WARNING:  This option is for rapid development ONLY and does NOT support\r\n' +
    'WARNING:  older browsers.  Make sure you test using: \r\n' +
    'WARNING:\r\n' +
    'WARNING:  $ npm run build\r\n' +
    'WARNING:\r\n' +
    'WARNING:  before committing your work. \r\n' +
    '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++', 'red'
  );
}

if (argv.hasOwnProperty('watch')) watch_js();


// ----------  Gulp Tasks  ---------- //

// Transpiles es6 scripts down to es5
gulp.task('transpile_js', () => {
  return gulp.src(path.resolve(__dirname, path.join(project.assets.dev, 'js', 'vanilla', '**', '*') + '.js'))
    .pipe( concat('_transpiled.js') ).on('error', (err) => {
      logger('JavaScript', 'ERROR: build failed at gulp concat', 'red');
      console.log(err);
    })
    .pipe( babel({ presets: ['es2015'] }) ).on('error', (err) => {
      logger('JavaScript', 'ERROR: build failed at gulp babel', 'red');
      logger('JavaScript', '       * This could be caused by a syntax error in your vanilla JS', 'red');
      console.log(err);
    })
    .pipe( gulp.dest(path.resolve(__dirname, path.join(project.assets.dev, 'js', 'tmp'))) );
});

// Normal JS build process.  Transpiles, concatenates and minifies.
// This process is used by prod.
gulp.task('build_js_default', () => {
  return gulp.src( get_paths(true) )
    .pipe( concat(config.output_name) ).on('error', (err) => {
      logger('JavaScript', 'ERROR: build failed at gulp concat', 'red');
      console.log(err);
    })
    .pipe( uglify() ).on('error', (err) => {
      logger('JavaScript', 'ERROR: build failed at gulp uglify', 'red');
      console.log(err);
    })
    .pipe( gulp.dest( path.resolve( __dirname, path.join(project.assets.dist, 'js') ) ) );
});

// Concatenates JS files only, good for rapid development.
// Make sure to double check your work the default build option.
gulp.task('build_js_fast', () => {
  return gulp.src( get_paths(false) )
    .pipe(concat(config.output_name)).on('error', (err) => {
      logger('JavaScript', 'ERROR: build failed at gulp concat', 'red');
      console.log(err);
    })
    .pipe( gulp.dest(path.resolve(__dirname, path.join(project.assets.dist, 'js'))) );
});

// Concatenates, transpiles but does not minify.  Good for debugging.
gulp.task('build_js_uncompressed', () => {
  return gulp.src( get_paths(true) )
    .pipe(concat(config.output_name)).on('error', (err) => {
      logger('JavaScript', 'ERROR: build failed at gulp concat', 'red');
      console.log(err);
    })
    .pipe( gulp.dest(path.resolve(__dirname, path.join(project.assets.dist, 'js'))) );
});

// Simply logs output to the console
gulp.task('log_output', () => {
  // delete the tmp folder if it exists
  del( path.resolve(__dirname, path.join(project.assets.dev, 'js', 'tmp')) );
  return logger('JavaScript', 'Generated ' + path.resolve(__dirname, path.join(project.assets.dist, 'js', config.output_name)), 'green');
});


// ----------  Asset Building  ---------- //

function build_js () {
  // Runs various gulp tasks in sequence.
  // This is important because it guarantees that tasks run
  // in the correct order, instead of in parallel.
  gulp.task('build', () => {
    sequence(
      'transpile_js',
      config.build_variant,
      'log_output'
      );
  });
  gulp.start('build');
}

function watch_js () {
  let _path = path.join(project.assets.dev, 'js');
  logger('JavaScript', 'Watching for changes in ' + _path + '/**/*', 'yellow');
  watcher(_path, {
    recursive: true,
    filter: (name) => {
      // ignore anything in the tmp directory
      let _path = new RegExp( path.resolve(__dirname, path.join(project.assets.dev, 'js', 'tmp')) );
      return ! _path.test(name);
    }
  }, (evt, name) => {
    if (evt == 'update') {
      logger('JavaScript', 'Change detected on "' + name + '"', 'green');
      build_js ();
    }
  });
}


// ----------  Exports  ---------- //

// Allows this script to be accessible from other scripts
module.exports = (args) => {
  if (args.hasOwnProperty('uncompressed') && args.uncompressed == true) config.build_variant = 'uncompressed';
  if (args.hasOwnProperty('fast') && args.fast == true) config.build_variant = 'fast';
  if (args.hasOwnProperty('watch') && args.watch == true) watch_js();
  build_js();
}

// Allows this script to work directly from the command line
if (!module.parent) build_js();




