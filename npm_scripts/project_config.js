#!/usr/bin/env node
'use strict';

//  DOC  config.js
//
//  Basic configuration used by various node scripts
//  for this project.

const path = require('path');

module.exports = {
  assets: {
    dev: path.resolve(__dirname, '../dev/assets/'),
    dist: path.resolve(__dirname, '../dist/assets/')
  }
}