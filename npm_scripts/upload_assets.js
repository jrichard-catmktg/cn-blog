#!/usr/bin/env node
'use strict';

const azure           = require('azure-storage'); //for interaction with Azure
const argv            = require('yargs').argv; // for parameters to this script
const recursive       = require('recursive-readdir');
const replace         = require('replace-string');
const path            = require('path');
const log             = require('./utils/logger.js');
const grep            = require('grepit');
const util            = require('util');
// Azure Blob Credentials
const access_keys     = {
  'dev': {
    'account': 'devcnstorage1',
    'secret': 'STvFq7GzMBo7Pm6Rujti0Y4G6cwv2cRnKoxQGa9Ped3TcfSILVmXWby6L2oRQZDng63zScpYcbVX4YRQ6VXvwA=='
  },
  'sqa': {
    'account': 'sqacnstorage1',
    'secret': 'loBoGXz9gdub+Q1pfssHpUaOcko02s33ampAaZd0vidwrCnQX97PYU1rwpbFhVQ//LDuJfg7BpsZYvP1XP9lbQ=='
  },
  'uat': {
    'account': 'uatcnstorage1',
    'secret': 'pRfO7R5HWKfFvzu8CJ7JaWcLUxYOq2xD6puzNM6GDfJM99kZ951gVawgdKIJ7RkljoQ5+EUUFQ1XXxsxa/YweA=='
  }
};
// Content type mapping
const content_types   = {
  '.css': 'text/css',
  '.js': 'text/javascript',
  '.png': 'image/png',
  '.jpg': 'image/jpeg',
  '.gif': 'image/gif',
  '.ico': 'image/x-icon',
  '.woff': 'application/x-font-woff',
  '.woff2': 'application/x-font-woff2',
  '.ttf': 'application/x-font-ttf',
  '.otf': 'application/x-font-otf',
  '.eot': 'application/vnd.ms-fontobject',
  '.svg': 'image/svg+xml'
};
const asset_directories = [
  'web/assets/js/',
  'web/assets/css/',
  'web/assets/img/',
  'web/assets/fonts/'];
const container_name  = 'assets';
const site_name       = 'ccjp';
const version_pattern = /\d\d\.\d\d\.\d\d/;
const version         = replace(version_pattern.exec(grep(/site_version:/, 'app/config/site.yml')).toString(), '.', '');
const env = argv.hasOwnProperty('env') ? argv.env : null;
if(env && version) {
  let blob_service = azure.createBlobService(access_keys[env].account, access_keys[env].secret);
  blob_service.createContainerIfNotExists(
    container_name,
    {publicAccessLevel: 'container'},
    function (error, result, response) {
      if(!error) {
        log('Azure',
            'Container \'' + container_name + ((result.created) ? '\' was created' : '\' already exists'),
            'yellow');
        // Set CORS blob properties and upload assets
        configure_blob_and_upload(blob_service);
      } else {
        log_error_and_exit(error);
      }
    });
} else {
  log_error_and_exit('Unable to determine environment or site version');
}

function configure_blob_and_upload(blob_service) {
  blob_service.getServiceProperties(function (error, result, response) {
    if(!error) {
      result.Cors = {
        'CorsRule': [{
          'AllowedMethods': ['GET'],
          'AllowedOrigins': ['*'],
          'AllowedHeaders': ['x-ms-blob-content-type', 'x-ms-blob-content-disposition'],
          'ExposedHeaders': ['x-ms-*'],
          'MaxAgeInSeconds': 86400
        }]
      };
      blob_service.setServiceProperties(result, function (error, result, response) {
        if(!error) {
          log('Azure', 'Cors properties for blob were set', 'yellow');
          upload_to_blob(asset_directories, blob_service);
        } else {
          log_error_and_exit(error);
        }
      });
    } else {
      log_error_and_exit(error);
    }
  });
}

function upload_to_blob(dir_paths, blob_service) {
  for(let dir_path of dir_paths) {
    recursive(dir_path, function (error, read_files) {
      if(!error) {
        for(let asset of read_files) {
          let ext = path.extname(asset);
          let type;
          if(content_types.hasOwnProperty(ext)) {
            switch(ext) {
              case '.css':
                type = 'css';
                break;
                
              case '.js':
                type = 'js';
                break;
                
              case '.png':
              case '.jpg':
              case '.gif':
              case '.ico':
                type = 'img';
                break;
                
              case '.woff':
              case '.woff2':
              case '.ttf':
              case '.otf':
              case '.eot':
              case '.svg':
                type = 'fonts';
                break;
                
              default:
                type = 'other';
            }
            blob_service.createBlockBlobFromLocalFile(
              container_name,
              site_name + '/' + version + '/' + type + '/' + replace(asset, 'web/assets/' + type + '/', ''),
              asset,
              function (error, result, response) {
                if(!error) {
                  log('Azure', 'Uploaded file: ' + result.name, 'green');
                } else {
                  log_error_and_exit(error);
                }
              });
          } else {
            log('Azure', 'Skipping file ' + asset + ' due to unapproved extension', 'red');
          }
        }
      } else {
        log_error_and_exit(error);
      }
    });
  }
}

function log_error_and_exit(error) {
  log('Azure', error.toString(), 'red');
  process.exit(1);
}