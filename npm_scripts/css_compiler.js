#!/usr/bin/env node
'use strict';


// DOC: css_compiler
//
// Compiles css using node-sass and places the
// output in /web/assets/css.
//
// USAGE:
//    CLI: $ node ./npm_scripts/css_compiler --debug (optional)
//
//    const css = require(./css_compiler);
//    css({
//         debug: true (optional) 
//    });
//
// OPTIONS:
//    debug (boolean) (optional) Setting to true will prevent CSS from getting minified.


// ----------  Setup  ---------- //

const argv = require('yargs').argv; // for parameters to this script
const path = require('path'); // for resolving absolute paths
const writeFile = require('write'); // for writing compiled files
const colors = require('colors'); // coloizing console output
const sass = require('node-sass'); // compiles CSS and SCSS
const watcher = require('node-watch'); // for watching files for changes
const files = require('./utils/get_files.js'); // for compiling multiple files
const project = require('./project_config.js'); // configuration specific to this project

var config = {
  debug: false, // (boolean) (optional) - Does not minify css when set to true.
}


// ----------  CLI  ---------- //

// Map argv properties to config properties
if (argv.hasOwnProperty('debug')) config.debug = true;
if (argv.hasOwnProperty('watch')) watch();

// Colorize console output
function log (str, col) {
  console.log( colors[col]('[CSS] ' + str) );
}

log('Compiling CSS', 'green');


// ----------  File Management  ---------- //

// writes the actual CSS file to disk.
function write_css (str, filename) {
  var filePath = path.join(project.assets.dist, 'css', filename);
  writeFile(filePath, str, (err) => {
    err ? log(err, 'red')
        : log('Generated ' + filePath, 'green');
  });
}

function watch () {
  let _path = path.join(project.assets.dev, 'css');
  log('Watching for changes in ' + _path + '/*/**', 'yellow');
  watcher(_path, {recursive: true}, (evt, name) => {
    if (evt == 'update') {
      log('Change detected on "' + name + '"', 'green');
      build_css(); 
    }
  });
}


// ----------  SASS  ---------- //

// Determines if output CSS should be minified or not.
function compression () {
  var cprsn = 'compressed'; // CSS file minification
  if (config.debug == true) {
    log('Debug option used, CSS will not be minified', 'yellow');
    cprsn = 'nested';
  }
  return cprsn;
}

// Actual SASS compilation.
// SEE:  https://www.npmjs.com/package/node-sass
function run_sass (input, output) {
  sass.render({
    file: path.join(project.assets.dev, 'css', input),
    includePaths: [ '*/**',
                    '../vendors/*/**'
                  ],
    outputStyle: compression()
  }, (err, result) => {
    if (err) {
      log('Sass failed to compile', 'red');
      log(err, 'red');
    } else {
      write_css(result.css.toString(), output);
    }
  });
}

// Gathers a list of scss files from which to build the css
function build_css () {
  var stylesheets = files( path.join(project.assets.dev, 'css'), '.scss' );
  if (stylesheets.length > 1) log('Looks like you\'re generating multiple CSS files, ' +
                                  'the page would be faster if you @import everything into ' +
                                  'one scss file to reduce server requests.', 
                                  'yellow');
  for (var i = 0; i < stylesheets.length; i++) {
    var newname = stylesheets[i].replace('.scss', '.css');
    run_sass(stylesheets[i], newname);
  }
}


// ----------  Exports  ---------- //

module.exports = (args) => {
  if (args.hasOwnProperty('debug') && args.debug == true) config.debug = true;
  if (args.hasOwnProperty('watch') && args.watch == true) watch();
  build_css();
}

if (!module.parent) build_css();

