#!/usr/bin/env node
'use strict';

//  DOC: get_files.js
//
//  Returns a list of file names, filtered by
//  extension in a given directory.
//
//  USAGE:
//    const getFile = requrie('./npm_scripts/utils/get_files.js);
//    getFile('path_to_file', '.ext');
//
//  NOTE:
//    Filenames beginning with "_" will be ignored.

const fs = require('fs');
const argv = require('yargs').argv;
const log = require('./logger.js');

function getFiles(path, ext) {
  var result = [];
  var files = fs.readdirSync(path);
  for (var i = 0; i < files.length; i++ ) {
    var file = files[i];
    if (file[0] != '_' && file.substr(file.length - ext.length, file.length) == ext) {
      result.push(file);
    }
  }
  if (!result.length) log('get_files', 'No files of extension "' + ext + '" found in "' + path + '"' , 'yellow');
  return result;
}


// ---------  Exports  ---------- //
module.exports = (path, ext) => {
  return getFiles(path, ext);
}


// ----------  CLI  ---------- //
// TODO: Finish this

if (!module.parent) {
  if (!argv.hasOwnProperty('path')) throw ('ERROR: get_files requires \'path\' argument');
  if (!argv.hasOwnProperty('ext')) throw ('ERROR: get_files requires \'ext\' argument');
  return getFiles(argv.path, argv.ext);
}