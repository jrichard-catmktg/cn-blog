#!/usr/bin/env node
'use strict';

//  DOC: logger.js
//
//  Helps standardize log formatting
//  node scripts by requiring a prefix, message and color
//
//  USAGE:
//    const log = require('./npm_scripts/utils/logger.js);
//    log('CSS', 'No valid css file was found', 'yellow');

const colors = require('colors');

// Colorize console output
function log (prefix, str, col) {
  console.log( colors[col]('[' + prefix + '] ' + str) );
}

module.exports = (prefix, str, col) => {
  return log (prefix, str, col);
}