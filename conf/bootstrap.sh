#!/bin/bash

function go {
  sudo apt-get -y upgrade
  sudo apt-get -y update
  
  # Apache setup
  sudo apt-get -y install apache2
  sudo cp /conf/apache2.conf /etc/apache2/
  sudo cp /conf/000-default.conf /etc/apache2/sites-available

  # Vim prefs
  sudo echo "syntax off" >> ~/.vimrc
  sudo echo "set number" >> ~/.vimrc

  # mySQL setup
  sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
  sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

  sudo apt-get -y install mysql-server
  
  echo "CREATE DATABASE wordpress" | mysql -uroot -proot
  
  # php5 setup (probably more than is necessary)
  sudo apt-get -y install php5 php5-common php5-dbg php5-json php5-mysql
  
  sudo service apache2 restart
  
  # Wrap it up!
  sudo cp /conf/motd /etc/motd

  echo ""
  echo "--------------------"
  echo "All set!"
  echo "--------------------"
  echo "Be sure to add the following entries to your hosts file:"
  echo "192.168.33.12 wordpress.local"
  echo ""
  echo "Visit wordpress.local to finish setup."
  echo "--------------------"
  echo ""
}

go